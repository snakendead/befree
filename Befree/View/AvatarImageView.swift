//
//  AvatarImageView.swift
//  Befree
//
//  Created by developer2 on 02.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import WebImage

class AvatarImageView: UIImageView {

    override var frame: CGRect {
        didSet {
            guard frame != CGRectNull else { return }
            recalculateSizes()
        }
    }
    
    let placeholderImage = UIImage(named: "")
    
    required init() {
        super.init(image: nil, highlightedImage: placeholderImage)
        backgroundColor = UIColor.clearColor()
        clipsToBounds = true
        contentMode = UIViewContentMode.ScaleAspectFill
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setUrl(url: NSURL) {
        sd_setImageWithURL(url, placeholderImage: placeholderImage,
                        options: [SDWebImageOptions.AllowInvalidSSLCertificates, SDWebImageOptions.ContinueInBackground],
                        progress: { (one, two) in
            
                        }, completed:  {(image, error, cacheType, finish) in
                            
                        })
    }
    
    private func recalculateSizes() {
        layer.cornerRadius = frame.width / 2
    }
}
