//
//  ProfilePreview.swift
//  Befree
//
//  Created by developer2 on 02.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class ProfilePreview: UIView {

    enum PreviewType {
        case Sidemenu
        case List
        case Post
    }
    
    var type: PreviewType = .Post
    var timeHidden: Bool = false
    
    var avatar: AvatarImageView!
    
    var titleLabel: UILabel!
    var statusLabel: UILabel!
    
    var dateLabel: UILabel!
    var timeLabel: UILabel!
    
    var created = false
    
    override var frame: CGRect {
        didSet {
            guard created == true else { return }
            recalculateSizes()
        }
    }
    
    required init() {
        super.init(frame: CGRectNull)
        firstInitialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        firstInitialize()
    }

    func firstInitialize() {
        backgroundColor = UIColor.clearColor()
        clipsToBounds = true
        
        avatar = AvatarImageView()
        addSubview(avatar)
        
        titleLabel = UILabel()
        titleLabel.backgroundColor = UIColor.clearColor()
        titleLabel.textAlignment = NSTextAlignment.Left
        titleLabel.font = MyFont.Medium
        addSubview(titleLabel)

        statusLabel = UILabel()
        statusLabel.backgroundColor = UIColor.clearColor()
        statusLabel.textAlignment = NSTextAlignment.Left
        statusLabel.font = MyFont.SmallThin
        addSubview(statusLabel)
        
        dateLabel = UILabel()
        dateLabel.backgroundColor = UIColor.clearColor()
        dateLabel.textAlignment = NSTextAlignment.Right
        dateLabel.font = MyFont.SmallThin
        addSubview(dateLabel)
        
        timeLabel = UILabel()
        timeLabel.backgroundColor = UIColor.clearColor()
        timeLabel.textAlignment = NSTextAlignment.Right
        timeLabel.font = MyFont.SmallThin
        addSubview(timeLabel)
        
        created = true
    }
    
    func setStyle(type: PreviewType, timeHidden: Bool) {
        self.type = type
        self.timeHidden = timeHidden
        
        let titleColorMap = [PreviewType.Sidemenu: MyColor.White, .List: MyColor.Black, .Post: MyColor.Cyan]
        let statusColorMap = [PreviewType.Sidemenu: MyColor.MiddleGray, .List: MyColor.MiddleGray, .Post: MyColor.MiddleGray]
        
        titleLabel.textColor = titleColorMap[type]
        statusLabel.textColor = statusColorMap[type]
        dateLabel.textColor = statusColorMap[type]
        timeLabel.textColor = statusColorMap[type]
        dateLabel.hidden = timeHidden
        timeLabel.hidden = timeHidden
    }
    
    func fillProfile(title: String, status: String, imageUrl: NSURL, time: NSDate?) {
        defer {
            recalculateSizes()
        }
        titleLabel.text = title
        statusLabel.text = status
        avatar.setUrl(imageUrl)

        guard time != nil else { return }
        
        timeLabel.text = timeFromDate(time!)
        dateLabel.text = dateFromDate(time!)
    }
    
    private func recalculateSizes() {
        let timeLabelWidth: CGFloat = timeHidden ? 0 : 70.0
        avatar.frame = CGRectMake(MySizes.offset, 0, frame.height, frame.height)

        let contentWidth: CGFloat = frame.width - CGRectGetMaxX(avatar.frame) + MySizes.offset*2 - timeLabelWidth
        let contentHeight: CGFloat = frame.height / 2
        titleLabel.frame = CGRectMake(CGRectGetMaxX(avatar.frame) + MySizes.offset, MySizes.smallOffset, contentWidth, contentHeight)
        statusLabel.frame = CGRectMake(CGRectGetMaxX(avatar.frame) + MySizes.offset, contentHeight, contentWidth, contentHeight)
        
        timeLabel.frame = CGRectMake(frame.width - timeLabelWidth, MySizes.offset, timeLabelWidth, contentHeight)
        dateLabel.frame = CGRectMake(frame.width - timeLabelWidth, contentHeight, timeLabelWidth, contentHeight)
    }
}
