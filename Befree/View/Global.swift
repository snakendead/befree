//
//  GlobalConstants.swift
//  Befree
//
//  Created by developer2 on 02.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

func safeString(string: String?) -> String {
    return string == nil ? "" : string!
}

struct MyColor {
    static let White = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    static let Cyan = UIColor(red: 83/255, green: 196/255, blue: 194/255, alpha: 1.0)
    static let LightGray = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
    static let MiddleGray = UIColor(red: 142/255, green: 142/255, blue: 142/255, alpha: 1.0)
    static let DarkGray = UIColor(red: 55/255, green: 53/255, blue: 63/255, alpha: 1.0)
    static let DarkLightGray = UIColor(red: 102/255, green: 99/255, blue: 114/255, alpha: 1.0)
    static let Black = UIColor(red: 5/255, green: 5/255, blue: 5/255, alpha: 1.0)
}

struct MyFont {
    static let neue = "HelveticaNeue"
    static let neueThin = "HelveticaNeue-Thin"
    
    static let Big = UIFont(name: MyFont.neue, size: 18.0)
    static let Medium = UIFont(name: MyFont.neue, size: 16.0)
    static let Small = UIFont(name: MyFont.neue, size: 13.0)

    static let BigThin = UIFont(name: MyFont.neueThin, size: 18.0)
    static let MediumThin = UIFont(name: MyFont.neueThin, size: 16.0)
    static let SmallThin = UIFont(name: MyFont.neueThin, size: 13.0)
}

struct MySizes {
    static let smallOffset: CGFloat = 5.0
    static let offset: CGFloat = 10.0
    static let bigOffset: CGFloat = 20.0
    static let topOffset: CGFloat = 30.0
    static let smallContentHeight: CGFloat = 30.0
    static let contentHeight: CGFloat = 40.0
    static let buttonHeight: CGFloat = 40.0
    static let buttonOffset: CGFloat = 8.0
    static let bigContentHeight: CGFloat = 50.0
    static let largeContentHeight: CGFloat = 60.0
    static let bigPhotoHeight: CGFloat = 100.0
    static let topViewHeight: CGFloat = 228.0
}

extension UIFont {
//    class func myMedium() -> UIFont {
//        return UIFont(name: "HelveticaNeue", size: 16.0)!
//    }
}

extension UIColor {
//    class func myRed() -> UIColor {
//        return UIColor(red: 1, green: 1, blue: 1, alpha: 1)
//    }
}

extension UIButton {
    
    func setBackgroundColor(backgroundColor: UIColor, state:UIControlState) {
        self.setBackgroundImage(imageFromColor(backgroundColor), forState: state)
    }
    
    func imageFromColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0, 0, 1, 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image;
    }
}

func timeFromDate(date: NSDate) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle
    dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
    return dateFormatter.stringFromDate(date)
}

func dateFromDate(date: NSDate) -> String {
    if NSCalendar.currentCalendar().isDateInToday(date) {
        return "Сегодня"
    } else if NSCalendar.currentCalendar().isDateInYesterday(date) {
        return "Вчера"
    } else {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        return dateFormatter.stringFromDate(date)
    }
}
