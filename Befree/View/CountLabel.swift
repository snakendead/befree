//
//  CountLabel.swift
//  Befree
//
//  Created by developer2 on 03.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class CountLabel: UILabel {

    enum CountLabelStyle {
        case Transparent
        case Cyan
        case None
    }
    
    override var frame: CGRect {
        didSet {
            guard frame != CGRectNull else { return }
            recalculateSizes()
        }
    }
    
    var count: Int = 0 {
        didSet {
            text = "\(count)"
            recalculateSizes()
        }
    }
    
    required init() {
        super.init(frame: CGRectNull)
        setStyle(CountLabelStyle.None)
    }
    
    func setStyle(style: CountLabelStyle) {
        font = MyFont.Small
        layer.masksToBounds = true
        text = "\(count)"
        textAlignment = NSTextAlignment.Center
        switch style {
        case .Transparent:
            backgroundColor = UIColor.clearColor()
            layer.borderColor = MyColor.MiddleGray.CGColor
            textColor = MyColor.MiddleGray
            layer.borderWidth = 1
        case .Cyan:
            textColor = MyColor.White
            backgroundColor = MyColor.Cyan
        case .None:
            hidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func recalculateSizes() {
        let oldFrame = frame
        layer.cornerRadius = oldFrame.height/2

        let attributedText = NSAttributedString(string: text!, attributes: [NSFontAttributeName:font])
        let textSize = attributedText.boundingRectWithSize(CGSizeMake(1000, 1000), options: NSStringDrawingOptions.UsesFontLeading, context: nil)
        let textWidth: CGFloat = textSize.width
        
        let newWidth: CGFloat = textWidth + MySizes.smallOffset*2
        let width: CGFloat = oldFrame.height > newWidth ? oldFrame.height : newWidth

        guard width != frame.width else { return }
        
        frame = CGRectMake(oldFrame.origin.x, oldFrame.origin.y, width, frame.height)
    }
}
