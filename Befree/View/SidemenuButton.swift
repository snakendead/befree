//
//  SidemenuButton.swift
//  Befree
//
//  Created by developer2 on 03.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class SidemenuButton: UIButton {

    override var frame: CGRect {
        didSet {
            guard frame != CGRectNull else { return }
            recalculateSizes()
        }
    }
    
    var countLabel: CountLabel?
    
    required init(image: UIImage, title: String, countLabelStyle: CountLabel.CountLabelStyle) {
        super.init(frame: CGRectNull)
        setImage(image, forState: UIControlState.Normal)
        setTitle(title, forState: UIControlState.Normal)
        firstInitialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        firstInitialize()
    }
    
    func firstInitialize() {
        titleLabel?.font = MyFont.MediumThin
        imageEdgeInsets = UIEdgeInsetsMake(0, MySizes.offset, 0, 0)
        titleEdgeInsets = UIEdgeInsetsMake(0, MySizes.offset*3, 0, MySizes.offset)
        contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        countLabel = CountLabel()
        addSubview(countLabel!)
    }
    
    func recalculateSizes() {
        let attributedText = NSAttributedString(string: safeString(titleLabel!.text), attributes: [NSFontAttributeName:titleLabel!.font])
        let textSize = attributedText.boundingRectWithSize(CGSizeMake(1000, 1000), options: NSStringDrawingOptions.UsesFontLeading, context: nil)
        let textWidth: CGFloat = textSize.width
        guard countLabel != nil else { return }
        countLabel!.frame = CGRectMake(textWidth + MySizes.offset*2 + titleEdgeInsets.left, MySizes.offset, frame.width - textWidth + MySizes.offset*2, frame.height - MySizes.offset*2)
    }
    
}
