import UIKit

class SidemenuViewController: BaseViewController, UISearchBarDelegate {

    var viewModel: SidemenuViewModel!
    
    @IBOutlet weak var profilePreview: ProfilePreview!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var feedButton: SidemenuButton!
    @IBOutlet weak var messagesButton: SidemenuButton!
    @IBOutlet weak var branchesButton: SidemenuButton!
    @IBOutlet weak var ratingsButton: SidemenuButton!
    @IBOutlet weak var tasksButton: SidemenuButton!
    @IBOutlet weak var favouriteButton: SidemenuButton!
    @IBOutlet weak var documentButton: SidemenuButton!
    @IBOutlet weak var photoDocumentButton: SidemenuButton!
    @IBOutlet weak var eventsButton: SidemenuButton!
    @IBOutlet weak var libraryButton: SidemenuButton!
    @IBOutlet weak var settingsButton: SidemenuButton!
    @IBOutlet weak var logoutButton: SidemenuButton!
    
    
    required init(viewModel: SidemenuViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "SidemenuViewController")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindActions()
        firstInitialize()
    }

    func firstInitialize() {
        
        searchBar.translucent = true
        searchBar.barTintColor = MyColor.DarkGray
        searchBar.backgroundColor = MyColor.DarkGray
        searchBar.tintColor = MyColor.White
        let textField = searchBar.valueForKey("searchField") as! UITextField
        textField.backgroundColor = MyColor.White.colorWithAlphaComponent(0.1)
        searchBar.barStyle = .Default
        searchBar.placeholder = "Поиск"
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = MyColor.DarkGray.CGColor
        searchBar.delegate = self
        
        feedButton.countLabel?.setStyle(CountLabel.CountLabelStyle.Transparent)
        feedButton.countLabel?.count = 121
        
        messagesButton.countLabel?.setStyle(CountLabel.CountLabelStyle.Transparent)
        feedButton.countLabel?.count = 21
        
        tasksButton.countLabel?.setStyle(CountLabel.CountLabelStyle.Cyan)
        tasksButton.countLabel?.count = 4
        
        profilePreview.setStyle(ProfilePreview.PreviewType.Sidemenu, timeHidden: true)
    }
    
    func bindViewModel() {
        viewModel.profile.producer
            .filter({ [unowned self] (profile) -> Bool in
                return profile != nil
                })
            .startWithNext { [unowned self] (profile) in
                let preview = profile!.avatar!.previewNormal!
                self.profilePreview.fillProfile(profile!.firstName! + " " + profile!.lastName!, status: profile!.userDepartment!.name!, imageUrl: NSURL(string: self.viewModel.viewModelService.service.imageUrl + preview)!, time: nil)
            }
    }
    
    func bindActions() {
        let tapGesture = UITapGestureRecognizer(target: self, action: nil)
        profilePreview.addGestureRecognizer(tapGesture)
        
        tapGesture.rac_gestureSignal()
            .toSignalProducer()
            .startWithNext({ [unowned self] _ in
                let vm = ProfileViewModel(viewModelService: self.viewModel.viewModelService)
                self.viewModel.viewModelService.setProfileNavigation(vm)
            })
        
        feedButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithNext({ [unowned self] _ in
                self.viewModel.viewModelService.setFeedNavigation()
            })
        
        branchesButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithNext { [unowned self] _ in
                self.viewModel.viewModelService.setBranchesNavigation()
            }
        
        logoutButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithNext { [unowned self] _ in
                self.viewModel.viewModelService.logout()
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        bindViewModel()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard created == false else { return }
        created = true
        recalculateSizes()
    }
    
    func recalculateSizes() {
//        let contentWidth: CGFloat = view.frame.width - MySizes.offset*8
//        let previewHeight: CGFloat = 50.0
//        profilePreview.frame = CGRectMake(MySizes.offset, MySizes.offset*4, contentWidth, previewHeight)
//        searchBar.frame = CGRectMake(MySizes.offset, CGRectGetMaxY(profilePreview.frame) + MySizes.offset, contentWidth, MySizes.contentHeight)
//
//        let buttonOffset: CGFloat = MySizes.buttonHeight// + MySizes.smallOffset
//
//        feedButton.frame = CGRectMake(MySizes.offset, CGRectGetMaxY(searchBar.frame) + MySizes.offset, contentWidth, MySizes.buttonHeight)
//        messagesButton.frame = CGRectOffset(feedButton.frame, 0, buttonOffset)
//        branchesButton.frame = CGRectOffset(messagesButton.frame, 0, buttonOffset)
//        ratingsButton.frame = CGRectOffset(branchesButton.frame, 0, buttonOffset)
//        tasksButton.frame = CGRectOffset(ratingsButton.frame, 0, buttonOffset)
//        favouriteButton.frame = CGRectOffset(tasksButton.frame, 0, buttonOffset)
//        documentButton.frame = CGRectOffset(favouriteButton.frame, 0, buttonOffset)
//        photoDocumentButton.frame = CGRectOffset(documentButton.frame, 0, buttonOffset)
//        eventsButton.frame = CGRectOffset(photoDocumentButton.frame, 0, buttonOffset)
//        libraryButton.frame = CGRectOffset(eventsButton.frame, 0, buttonOffset)
    }
}
