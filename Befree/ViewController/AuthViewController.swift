import UIKit
import ReactiveCocoa

class AuthViewController: BaseViewController {

    var viewModel: AuthViewModel!
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var getPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginConstraint: NSLayoutConstraint!
    
    init(viewModel: AuthViewModel) {
        super.init(nibName: "AuthViewController")
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindViewModel()
        bindActions()
    }
    
    func firstInitialize() {
        
    }
    
    func bindViewModel() {
        let showProducer = NSNotificationCenter.defaultCenter()
            .rac_addObserverForName(UIKeyboardWillShowNotification, object: nil)
            .toSignalProducer()
            .map({ (next) -> NSNotification in
                return next as! NSNotification
            })
            .producer

        let hideProducer = NSNotificationCenter.defaultCenter()
            .rac_addObserverForName(UIKeyboardWillHideNotification, object: nil)
            .toSignalProducer()
            .map({ (next) -> NSNotification in
                return next as! NSNotification
            })
            .producer

        let (signal, observer) = SignalProducer<SignalProducer<NSNotification, NSError>, NSError>.buffer(5)
        
        observer.sendNext(showProducer)
        observer.sendNext(hideProducer)

        signal.flatten(.Merge)
            .on(next: { [weak self] notification in
                    guard let self_ = self else { return }
                    guard let kbSizeValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }
                    guard let kbDurationNumber = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber else { return }
                    print(kbSizeValue)
                    print(kbDurationNumber)
                    self_.animateConstraint(kbSizeValue.CGRectValue().origin.y, duration: kbDurationNumber.doubleValue)
            })
            .start()

    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func bindActions() {
        loginButton.rac_signalForControlEvents(.TouchUpInside)
            .toSignalProducer()
            .startWithNext {
                [unowned self]
                _ in
                self.view.endEditing(true)
                self.loginReact()
            }
    }
    
    func loginReact() {
        viewModel.viewModelService.service.auth(phoneTextField.text!, password: passwordTextField.text!)
            .on(failed: { [unowned self] (error) in
                self.loginFailReact()
            }, completed: { [unowned self] Void in
                self.viewModel.viewModelService.setRootMainContainer()
            }, next: { (token) in
                NSUserDefaults.standardUserDefaults().setObject(token, forKey: tokenKey)
            }).start()
    }
    
    func loginFailReact() {
        let alert = UIAlertController(title: "Ошибка", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(action)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func animateConstraint(origin: CGFloat, duration: Double) {
        loginConstraint.constant = (view.frame.height - origin) * -1.0
        UIView.animateWithDuration(duration) { 
            self.view.layoutIfNeeded()
        }
    }
}
