import UIKit
import ReactiveCocoa

class BranchesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var bindingHelper: TableViewBindingHelper!
    
    var viewModel: BranchesViewModel!
    
    init(viewModel: BranchesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "BranchesViewController")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindActions()
    }
    
    func firstInitialize() {
        tableView.tableFooterView = UIView()
        tableView.tableHeaderView = UIView()
    }
    
    func bindActions() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        title = "Отделы"
        bindViewModel()
    }
    
    func bindViewModel() {
        let producer = viewModel.data.producer.mapError { (noError) -> NSError in
            return NSError(domain: "", code: 0, userInfo: nil)
        }.map { (branches) -> [AnyObject] in
            let array: [AnyObject] = branches
            return array
        }
        bindingHelper = TableViewBindingHelper(tableView: tableView,
                                               data: producer,
                                               nibName: "BranchCell")
        bindingHelper.selectProducer.on(next: {
            [unowned self]
            (indexPath) in
            let branch = self.viewModel.data.value[indexPath.row]
            let vm = BranchViewModel(viewModelService: self.viewModel.viewModelService, branch: branch)
            self.viewModel.viewModelService.pushBranch(vm)
        }).start()
    }
}
