
import UIKit
import ReactiveCocoa

class ProfileViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    var user = MutableProperty<ProfileEntity?>(nil)
    
    init(viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
        viewModelService.service.getCurrentUser()
            .startWithNext { [unowned self] (profile) in
                guard profile != nil else { return }
                self.user.value = profile!
            }
    }
}
