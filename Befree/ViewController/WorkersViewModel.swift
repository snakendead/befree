
import UIKit
import ReactiveCocoa

class WorkersViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    var branch = MutableProperty<BranchEntity>(BranchEntity.MR_createEntity()!)
    var data = MutableProperty<[ProfileEntity]>([ProfileEntity]())

    init(viewModelService: ViewModelServiceProtocol, branch: BranchEntity) {
        self.branch.value = branch
        self.viewModelService = viewModelService
        super.init()
        viewModelService.service.getBranchWorkers(Int(branch.id!.intValue))
            .startWithNext {
                [unowned self]
                (profiles) in
                self.data.value.appendContentsOf(profiles)
        }
    }
}
