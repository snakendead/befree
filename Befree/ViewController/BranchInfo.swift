import UIKit
import ReactiveCocoa

class BranchInfoViewController: BaseViewController {

    var viewModel: BranchInfoViewModel!
    
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet weak var workersButton: UIButton!
    @IBOutlet weak var workersLabel: UILabel!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var filesButton: UIButton!
    @IBOutlet weak var filesLabel: UILabel!
    
    
    init(viewModel: BranchInfoViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "BranchInfoViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindActions()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func firstInitialize() {

    }
    
    func bindViewModel() {
        viewModel.branch.producer
            .startWithNext { [unowned self] _ in
                self.fillBranchInfo()
        }
    }
    
    func fillBranchInfo() {
        aboutTextView.text = viewModel.branch.value.descript
        workersLabel.text = "\(viewModel.branch.value.numberOfEmployees!.intValue)"
    }
    
    func bindActions() {
        workersButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithNext {
                [unowned self]
                _ in
                let viewModel = WorkersViewModel(viewModelService: self.viewModel.viewModelService, branch: self.viewModel.branch.value)
                self.viewModel.viewModelService.pushBranchWorkers(viewModel)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        title = "Информация"
        bindViewModel()
    }
}
