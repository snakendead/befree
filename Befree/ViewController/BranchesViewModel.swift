
import UIKit
import ReactiveCocoa

class BranchesViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    var data = MutableProperty<[BranchEntity]>([BranchEntity]())
    
    init(viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
        viewModelService.service.getBranches()
            .startWithNext { [unowned self] (branches) in
                self.data.value.appendContentsOf(branches)
            }
    }
}
