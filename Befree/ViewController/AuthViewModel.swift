
import UIKit
import ReactiveCocoa

class AuthViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    init(viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
    }
}
