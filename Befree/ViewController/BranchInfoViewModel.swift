
import UIKit
import ReactiveCocoa

class BranchInfoViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    var branch = MutableProperty<BranchEntity>(BranchEntity.MR_createEntity()!)
    
    init(viewModelService: ViewModelServiceProtocol, branchId: Int) {
        self.viewModelService = viewModelService
        super.init()
        viewModelService.service.getBranch(branchId)
            .startWithNext { [unowned self] (branch) in
                guard branch != nil else { return }
                self.branch.value = branch!
        }
    }
}
