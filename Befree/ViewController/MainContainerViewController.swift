import UIKit
import Foundation
import ReactiveCocoa

class MainContainerViewController: BaseViewController {

    var viewModel: MainContainerViewModel!
    
    var sidemenuVC: SidemenuViewController!
    var contentNavigationController: UINavigationController?
    
    var contentViewTransform: CGAffineTransform?
    var swipeCloseGesture: UISwipeGestureRecognizer!
    var tapCloseGesture: UITapGestureRecognizer!
    var swipeOpenGesture: UISwipeGestureRecognizer!
    
    var sideMenuDisposes = Array<RACDisposable>()

    var sideMenuShown = false {
        didSet {
            swipeCloseGesture.enabled = sideMenuShown
            tapCloseGesture.enabled = sideMenuShown
            swipeOpenGesture.enabled = !sideMenuShown
        }
    }
    
    init(viewModel: MainContainerViewModel) {
        let sidemenuVM = SidemenuViewModel(viewModelService: viewModel.viewModelService)
        sidemenuVC = SidemenuViewController(viewModel: sidemenuVM)
        self.viewModel = viewModel
        super.init(nibName: "MainContainerViewController")
        firstInitialize()
        bindActions()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func firstInitialize() {
        view.backgroundColor = UIColor.whiteColor()
        addChildViewController(sidemenuVC)
        view.addSubview(sidemenuVC.view)
    }
    
    func bindViewModel() {
        
    }
    
    func bindActions() {
        swipeCloseGesture = UISwipeGestureRecognizer(target: self, action: nil)
        swipeCloseGesture.direction = .Left
        swipeCloseGesture.rac_gestureSignal()
            .toSignalProducer()
            .startWithNext({ [unowned self] _ in
                self.hideSidemenu()
            })
        
        swipeOpenGesture = UISwipeGestureRecognizer(target: self, action: nil)
        swipeOpenGesture.direction = .Right
        swipeOpenGesture.rac_gestureSignal()
            .toSignalProducer()
            .map({ (next) -> UISwipeGestureRecognizer in
                return next as! UISwipeGestureRecognizer
            })
            .map({ [unowned self] (recognizer) -> CGPoint in
                return recognizer.locationInView(self.contentNavigationController?.view)
            })
            .filter({ (point) -> Bool in
                return point.x <= 10
            })
            .startWithNext({ [unowned self] _ in
                self.showSidemenu()
            })

        tapCloseGesture = UITapGestureRecognizer(target: self, action: nil)
        tapCloseGesture.rac_gestureSignal()
            .toSignalProducer()
            .startWithNext({ [unowned self] _ in
                self.hideSidemenu()
            })

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = ""
        bindViewModel()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard created == false else { return }
        created = true
        recalculateSizes()
    }
    
    func recalculateSizes() {
        sidemenuVC.view.frame = view.frame
        contentNavigationController?.view.frame = view.frame
    }
    
    func setNavigationViewController(navigationViewController: UINavigationController) {

        let oldNavController = contentNavigationController
        
        contentNavigationController = navigationViewController
        prepare(contentNavigationController!)
        contentNavigationController!.view.layer.opacity = 0.0
        addChildViewController(contentNavigationController!)
        view.addSubview(contentNavigationController!.view)
        view.sendSubviewToBack(sidemenuVC.view)

        contentViewTransform = contentNavigationController?.view.transform
        contentNavigationController?.view.addGestureRecognizer(swipeCloseGesture)
        contentNavigationController?.view.addGestureRecognizer(tapCloseGesture)
        contentNavigationController?.view.addGestureRecognizer(swipeOpenGesture)
    
        if oldNavController != nil {
            contentNavigationController?.view.transform = oldNavController!.view.transform
            contentNavigationController?.view.center.x = oldNavController!.view.center.x
            if sideMenuShown == true { hideSidemenu() }
            
            UIView.animateWithDuration(0.1,
                                       delay: 0.0,
                                       options: UIViewAnimationOptions.CurveEaseInOut,
                                       animations: {
                                        self.contentNavigationController!.view.layer.opacity = 1.0
            }) { (finish) in
                guard finish == true else { return }
                self.destroyContentNavigation(oldNavController)
            }
        } else {
            if sideMenuShown == true { hideSidemenu() }
            self.contentNavigationController!.view.layer.opacity = 1.0
        }
        
    }
    
    private func prepare(navigationController: UINavigationController) {
        navigationController.navigationBar.barTintColor = MyColor.Cyan
        navigationController.navigationBar.backgroundColor = MyColor.Cyan
        navigationController.navigationBar.tintColor = UIColor.whiteColor()
        navigationController.navigationBar.translucent = false
        navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(),
                                                                  NSFontAttributeName: MyFont.Medium!]
        navigationController.navigationItem.backBarButtonItem?.title = ""
        navigationController.navigationBar.setBackgroundImage(UIImage(), forBarPosition: UIBarPosition.Any, barMetrics: UIBarMetrics.Default)
        navigationController.navigationBar.shadowImage = UIImage()

        let menu = UIBarButtonItem(image: UIImage(named: "menu"), style: UIBarButtonItemStyle.Plain,
                                   target: self, action: Selector())
        menu.tintColor = UIColor.whiteColor()
        menu.rac_command = RACCommand(signalBlock: {
            [weak self]
            (next) -> RACSignal! in
            guard let self_ = self else { return RACSignal.empty() }
            self_.switchMenu()
            return RACSignal.empty()
        })
        
        navigationController.viewControllers.first!.navigationItem.leftBarButtonItem = menu
        let backItem = navigationController.navigationItem.backBarButtonItem
        backItem?.title = "Назад"
    }
    
    private func destroyContentNavigation(navController: UINavigationController?) {
        guard let contentNC = navController else { return }
        contentNC.willMoveToParentViewController(nil)
        contentNC.view.removeFromSuperview()
        contentNC.removeFromParentViewController()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    func switchMenu() {
        sideMenuShown ? hideSidemenu() : showSidemenu()
    }
    
    func showSidemenu() {
        UIView.animateWithDuration(0.33, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.contentNavigationController?.view.transform = CGAffineTransformScale(self.contentViewTransform!, 0.7, 0.7)
            self.contentNavigationController?.view.center.x += self.view.frame.width/9*6
        }) { (finish) in
            self.sideMenuShown = true
        }
    }
    
    func hideSidemenu() {
        sidemenuVC.view.endEditing(true)
        UIView.animateWithDuration(0.33, delay: 0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
            self.contentNavigationController?.view.transform = CGAffineTransformScale(self.contentViewTransform!, 1.0, 1.0)
            self.contentNavigationController?.view.center.x = self.view.center.x
        }) { (finish) in
            self.sideMenuShown = false
        }
    }
}
