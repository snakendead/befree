import UIKit

class FeedViewController: BaseViewController {

    var viewModel: FeedViewModel!
    
    init(viewModel: FeedViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "FeedViewController")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindActions()
    }
    
    func firstInitialize() {

    }
    
    func bindActions() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        title = "Общая лента"
        bindViewModel()
    }
    
    func bindViewModel() {

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard created == false else { return }
        created = true
        recalculateSizes()
    }
    
    func recalculateSizes() {
        
    }
}
