
import UIKit
import ReactiveCocoa

class BranchViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    var data = MutableProperty<[PostEntity]>([PostEntity]())
    var branch: BranchEntity
    
    init(viewModelService: ViewModelServiceProtocol, branch: BranchEntity) {
        self.branch = branch
        self.viewModelService = viewModelService
        super.init()
    }
}
