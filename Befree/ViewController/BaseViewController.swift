//
//  BaseViewController.swift
//  Befree
//
//  Created by developer2 on 02.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var created = false
    
    init(nibName: String) {
        super.init(nibName: nibName, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
