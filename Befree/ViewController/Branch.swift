import UIKit
import WebImage

class BranchViewController: BaseViewController, UITableViewDelegate {

    var viewModel: BranchViewModel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var headerCell: BranchHeaderView?

    var bindingHelper: TableViewBindingHelper!

    init(viewModel: BranchViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "BranchViewController")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindActions()
    }

    func firstInitialize() {
        let headerReuse = "BranchHeaderView"
        let nibHeader = UINib(nibName: headerReuse, bundle: NSBundle.mainBundle())
        tableView.registerNib(nibHeader, forHeaderFooterViewReuseIdentifier: headerReuse)
        let objects = NSBundle.mainBundle().loadNibNamed(headerReuse, owner: self, options: nil)
        headerCell = objects.first as? BranchHeaderView
        tableView.tableFooterView = UIView()
    }
    
    func bindViewModel() {
        let producer = viewModel.data.producer.mapError { (noError) -> NSError in
            return NSError(domain: "", code: 0, userInfo: nil)
        }.map { (branches) -> [AnyObject] in
            let array: [AnyObject] = branches
            return array
        }
        bindingHelper = TableViewBindingHelper(tableView: tableView,
                                               data: producer,
                                               nibName: "FeedCell",
                                               header: headerCell)
        
        bindingHelper.selectProducer.on(next: {
            [unowned self]
            (indexPath) in

        }).start()
        
        title = viewModel.branch.name
        headerCell?.statusLabel.text = "Участников \(viewModel.branch.numberOfEmployees!.intValue)"
        guard let preview = viewModel.branch.avatar?.previewNormal else { return }
        guard let url = NSURL(string: viewModel.viewModelService.service.imageUrl + preview) else { return }
        headerCell?.profileImageView.sd_setImageWithURL(url, placeholderImage: nil, options: SDWebImageOptions.ProgressiveDownload)

    }
    
    func bindActions() {
        headerCell?.infoButton.rac_signalForControlEvents(UIControlEvents.TouchUpInside)
            .toSignalProducer()
            .startWithNext({ [unowned self] _ in
                let vm = BranchInfoViewModel(viewModelService: self.viewModel.viewModelService, branchId: Int(self.viewModel.branch.id!.intValue))
                self.viewModel.viewModelService.pushBranchInfo(vm)
            })
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        bindViewModel()
    }
}
