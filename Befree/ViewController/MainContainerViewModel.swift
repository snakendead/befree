
import UIKit
import ReactiveCocoa

class MainContainerViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    init(viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
    }
}
