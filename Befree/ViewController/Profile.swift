import UIKit
import WebImage

class ProfileViewController: BaseViewController {

    var viewModel: ProfileViewModel!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!

    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var sendMessageButton: UIButton!
    
    @IBOutlet weak var branchLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    
    @IBOutlet weak var placeButton: UIButton!
    @IBOutlet weak var scheduleButton: UIButton!
    
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet weak var aboutConstraint: NSLayoutConstraint!
    
    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "ProfileViewController")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindActions()
    }

    func firstInitialize() {
        profileImage.clipsToBounds = true
        profileImage.layer.cornerRadius = 50.0
        statusLabel.text = ""
    }
    
    func bindViewModel() {
        viewModel.user.producer
            .filter({ [unowned self] (profile) -> Bool in
                return profile != nil
            })
            .startWithNext { [unowned self] (profile) in
                self.fillInfo(profile!)
            }
    }
    
    func fillInfo(profile: ProfileEntity) {
        title = profile.firstName! + " " + profile.lastName!
        branchLabel.text = profile.userDepartment?.name
        phoneLabel.text = profile.phoneNumber
        mailLabel.text = profile.email
        aboutTextView.text = profile.descript
        positionLabel.text = profile.userPosition?.name
        branchLabel.text = profile.userDepartment?.name
        guard let preview = profile.avatar?.previewNormal else { return }
        guard let url = NSURL(string: viewModel.viewModelService.service.imageUrl + preview) else { return }
        profileImage.sd_setImageWithURL(url, placeholderImage: nil, options: SDWebImageOptions.ProgressiveDownload)
        
    }
    
    func bindActions() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarPosition: UIBarPosition.Any, barMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        title = "Профиль"
        bindViewModel()
    }
}
