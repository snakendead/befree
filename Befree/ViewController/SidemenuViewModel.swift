
import UIKit
import ReactiveCocoa

class SidemenuViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol
    
    var profile = MutableProperty<ProfileEntity?>(nil)
    
    init(viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
        viewModelService.service.getCurrentUser()
            .startWithNext { [unowned self] (profile) in
                guard profile != nil else { return }
                self.profile.value = profile!
        }
    }
}
