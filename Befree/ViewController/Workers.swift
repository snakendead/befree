import UIKit

class WorkersViewController: BaseViewController {

    var viewModel: WorkersViewModel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var bindingHelper: TableViewBindingHelper!
    
    init(viewModel: WorkersViewModel) {
        self.viewModel = viewModel
        super.init(nibName: "WorkersViewController")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstInitialize()
        bindActions()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func firstInitialize() {
        
    }
    
    func bindViewModel() {
        let producer = viewModel.data.producer.mapError { (noError) -> NSError in
            return NSError(domain: "", code: 0, userInfo: nil)
            }.map { (branches) -> [AnyObject] in
                let array: [AnyObject] = branches
                return array
        }
        bindingHelper = TableViewBindingHelper(tableView: tableView,
                                               data: producer,
                                               nibName: "WorkerCell")
        
        bindingHelper.selectProducer.on(next: {
            [unowned self]
            (indexPath) in
            
        }).start()
        
        title = "Участников \(viewModel.branch.value.numberOfEmployees!.intValue)"
    }
    
    func bindActions() {
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        bindViewModel()
    }
}
