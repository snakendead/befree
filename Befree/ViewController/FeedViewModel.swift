
import UIKit
import ReactiveCocoa

class FeedViewModel: BaseViewModel {
    
    var viewModelService: ViewModelServiceProtocol

    init(viewModelService: ViewModelServiceProtocol) {
        self.viewModelService = viewModelService
        super.init()
    }
}
