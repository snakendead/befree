//
//  OriginalImageEntity+CoreDataProperties.swift
//  Befree
//
//  Created by developer2 on 12.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension OriginalImageEntity {

    @NSManaged var id: NSNumber?
    @NSManaged var name: String?
    @NSManaged var originalName: String?
    @NSManaged var path: String?
    @NSManaged var mimeType: String?
    @NSManaged var size: NSNumber?
    @NSManaged var created: String?
    @NSManaged var temporary: String?
    @NSManaged var width: NSNumber?
    @NSManaged var height: NSNumber?
    @NSManaged var imageType: String?
    @NSManaged var owner: ProfileEntity?

}
