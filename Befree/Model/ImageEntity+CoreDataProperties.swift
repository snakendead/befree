//
//  ImageEntity+CoreDataProperties.swift
//  Befree
//
//  Created by developer2 on 12.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ImageEntity {

    @NSManaged var created: String?
    @NSManaged var previewSmall: String?
    @NSManaged var previewNormal: String?
    @NSManaged var previewLarge: String?
    @NSManaged var cropTopLeftX: NSNumber?
    @NSManaged var cropTopLeftY: NSNumber?
    @NSManaged var cropBottomRightX: NSNumber?
    @NSManaged var cropBottomRightY: NSNumber?
    @NSManaged var cropParams: CropParamsEntity?
    @NSManaged var originalImage: OriginalImageEntity?

}
