//
//  CropParamsEntity+CoreDataProperties.swift
//  Befree
//
//  Created by developer2 on 12.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CropParamsEntity {

    @NSManaged var topLeftX: NSNumber?
    @NSManaged var topLeftY: NSNumber?
    @NSManaged var bottomRightX: NSNumber?
    @NSManaged var bottomRightY: NSNumber?

}
