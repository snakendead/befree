//
//  ProfileEntity+CoreDataProperties.swift
//  Befree
//
//  Created by developer2 on 17.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension ProfileEntity {

    @NSManaged var descript: String?
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var id: String?
    @NSManaged var isActive: NSNumber?
    @NSManaged var lastName: String?
    @NSManaged var login: String?
    @NSManaged var patronymic: String?
    @NSManaged var phoneNumber: String?
    @NSManaged var userRole: String?
    @NSManaged var avatar: ImageEntity?
    @NSManaged var lastPostRead: PostEntity?
    @NSManaged var userDepartment: BranchEntity?
    @NSManaged var userPosition: UserPositionEntity?

}
