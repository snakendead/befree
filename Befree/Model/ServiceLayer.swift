//
//  ServiceLayer.swift
//  Befree
//
//  Created by developer2 on 12.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import ReactiveCocoa

class ServiceLayer: NSObject {
    
    let imageUrl = "http://social.befree.ddgcorp.ru:11235"
    let baseUrl = "http://dev.ddgcorp.ru:11235/api/v1/"
    
    enum URLTypes: String {
        case Users = "users/"
        case Current = "current/"
        case Paged = "paged/"
        case Departments = "departments/"
        case Login = "login/"
        case Logout = "logout/"
        case Files = "files/"
        case Upload = "upload/"
        case Avatar = "avatar/"
    }
    
    var dataProvider: DataProvider = DataProvider()
    
    func auth(login: String, password: String) -> SignalProducer<String, NSError> {
        let url = baseUrl + URLTypes.Users.rawValue + URLTypes.Login.rawValue
        let params = ["login": login,
                      "password": password]
        let producer = dataProvider.requestProducer(url,
                                                    params: params,
                                                    actionType: DataProvider.ActionType.Post,
                                                    auth: false)
        return producer.map({
            token -> String in
            return token as! String
        })
    }
    
    func getCurrentUser() -> SignalProducer<ProfileEntity?, NSError> {
        let url = baseUrl + URLTypes.Users.rawValue + URLTypes.Current.rawValue
        let producer = dataProvider.requestProducer(url,
                                                    params: nil,
                                                    actionType: DataProvider.ActionType.Get,
                                                    auth: true)
        return producer.map({
            (next) -> ProfileEntity? in
            guard let dict = next as? [String: AnyObject] else { return nil }
            guard let data = dict["data"] as? [String: AnyObject] else { return nil }
            let profile = ProfileEntity.MR_createEntity()
            profile?.safeSetValuesForKeysWithDictionary(data)
            return profile
        })
    }

    func getBranchWorkers(branchId: Int) -> SignalProducer<[ProfileEntity], NSError> {
        let url = baseUrl + URLTypes.Users.rawValue + URLTypes.Paged.rawValue
        let params = ["department": branchId]
        let producer = dataProvider.requestProducer(url,
                                                    params: params,
                                                    actionType: DataProvider.ActionType.Get,
                                                    auth: true)
        return producer.map({
            (next) -> [ProfileEntity] in
            guard let dict = next as? [String: AnyObject] else { return [] }
            guard let data = dict["data"] as? [String: AnyObject] else { return [] }
            guard let elements = data["elements"] else { return [] }
            guard let elements_ = elements as? [[String: AnyObject]] else { return [] }
            var entities = [ProfileEntity]()
            for element in elements_ {
                let entity = ProfileEntity.MR_createEntity()!
                entity.safeSetValuesForKeysWithDictionary(element)
                entities.append(entity)
            }
            return entities
        })
    }

    func getBranch(branchId: Int) -> SignalProducer<BranchEntity?, NSError> {
        let url = baseUrl + URLTypes.Departments.rawValue + "\(branchId)"
        let producer = dataProvider.requestProducer(url,
                                                    params: nil,
                                                    actionType: DataProvider.ActionType.Get,
                                                    auth: true)
        return producer.map({
            (next) -> BranchEntity? in
            guard let dict = next as? [String: AnyObject] else { return nil }
            guard let data = dict["data"] as? [String: AnyObject] else { return nil }
            let entity = BranchEntity.MR_createEntity()
            entity?.safeSetValuesForKeysWithDictionary(data)
            return entity
        })
    }

    
    func getBranches() -> SignalProducer<[BranchEntity], NSError> {
        let url = baseUrl + URLTypes.Departments.rawValue + URLTypes.Paged.rawValue
        let producer = dataProvider.requestProducer(url,
                                                    params: nil,
                                                    actionType: DataProvider.ActionType.Get,
                                                    auth: true)
        return producer.map({
            (next) -> [BranchEntity] in
            guard let dict = next as? [String: AnyObject] else { return [] }
            guard let data = dict["data"] as? [String: AnyObject] else { return [] }
            guard let elements = data["elements"] else { return [] }
            guard let elements_ = elements as? [[String: AnyObject]] else { return [] }
            guard let metadata = dict["metaData"] else { return [] }
            guard let metadata_ = metadata as? [String: AnyObject] else { return [] }
            guard let userId = metadata_["userId"] else { return [] }
            guard let userIdInt = userId as? Int else { return [] }
            
            var branches = [BranchEntity]()
            for element in elements_ {
                print(element["name"])
                let branch = BranchEntity.MR_createEntity()!
                branch.safeSetValuesForKeysWithDictionary(element)
                if branch.id?.intValue == Int32(userIdInt) { branch.myBranch = true }
                branches.append(branch)
            }
            return  branches
        })
    }

//    func getWorkers() -> SignalProducer<[ProfileEntity], DataProvider.APIError> {
//        let url = baseUrl + URLTypes.Departments.rawValue + URLTypes.Paged.rawValue
//        let producer = dataProvider.requestProducer(url,
//                                                    params: nil,
//                                                    actionType: DataProvider.ActionType.Get,
//                                                    auth: true)
//        return producer.map({
//            (next) -> [BranchEntity] in
//            guard let dict = next as? [String: AnyObject] else { return [] }
//            guard let data = dict["data"] as? [String: AnyObject] else { return [] }
//            guard let elements = data["elements"] else { return [] }
//            guard let elements_ = elements as? [[String: AnyObject]] else { return [] }
//            guard let metadata = dict["metaData"] else { return [] }
//            guard let metadata_ = metadata as? [String: AnyObject] else { return [] }
//            guard let userId = metadata_["userId"] else { return [] }
//            guard let userIdInt = userId as? Int else { return [] }
//            
//            var branches = [BranchEntity]()
//            for element in elements_ {
//                print(element["name"])
//                let branch = BranchEntity.MR_createEntity()!
//                branch.safeSetValuesForKeysWithDictionary(element)
//                if branch.id?.intValue == Int32(userIdInt) { branch.myBranch = true }
//                branches.append(branch)
//            }
//            return  branches
//        })
//    }
}


