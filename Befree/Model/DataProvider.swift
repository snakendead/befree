//
//  DataProvider.swift
//  Befree
//
//  Created by developer2 on 12.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit
import ReactiveCocoa
import Alamofire
import SwiftyJSON
import CoreData

class DataProvider: NSObject {

    enum APIError: ErrorType {
        case URLFailed
        case RequestFailed
        case RequestError
        case SerializationFailed
    }
    
    enum ActionType: String {
        case Get = "GET"
        case Post = "POST"
        case Put = "PUT"
        case Delete = "DELETE"
    }

    func requestProducer(urlString: String,
                         params: [String: AnyObject]?,
                         actionType: ActionType,
                         auth: Bool = true,
                         codes: [Int] = []) -> SignalProducer<AnyObject?, NSError> {
        
        return SignalProducer { observer, disposable in
            var successCodes = [200, 201, 202]
            successCodes.appendContentsOf(codes)
            guard let url = NSURL(string: urlString) else {
                let error = NSError(domain: "Ошибка URL", code: 1001, userInfo: nil)
                observer.sendFailed(error)
                return
            }
            var headers = [String: String]()
            var encoding: ParameterEncoding = ParameterEncoding.URLEncodedInURL
            if actionType != ActionType.Get {
                if params != nil {
                    headers["Content-Type"] = "application/json"
                    encoding = ParameterEncoding.JSON
                }
            }
            if auth == true {
                let token = NSUserDefaults.standardUserDefaults().valueForKey(tokenKey)
                headers["x-auth-token"] = "\(token as! String)"
            }
            let method = [ActionType.Get: Alamofire.Method.GET,
                ActionType.Post: Alamofire.Method.POST,
                ActionType.Put: Alamofire.Method.PUT,
                ActionType.Delete: Alamofire.Method.DELETE]

            let request = Alamofire.request(method[actionType]!,
                url,
                parameters: params,
                encoding: encoding,
                headers: headers)
            
            request
                .validate({ (request, response) -> Request.ValidationResult in
                    let status = response.statusCode
                    guard !successCodes.contains(status) else { return Request.ValidationResult.Success }
                    var errorDomain = ""
                    switch status {
                    case 0..<300:
                        return Request.ValidationResult.Success
                    case 400:
                        errorDomain = "Плохой, неверный запрос"
                    case 401:
                        errorDomain = "Не авторизован"
                    case 403:
                        errorDomain = "Протухший токен"
                    case 405:
                        errorDomain = "Метод не поддерживается"
                    case 409:
                        errorDomain = "Конфликт"
                    case 412:
                        errorDomain = "Условие ложно"
                    case 413:
                        errorDomain = "Размер запроса слишком велик"
                    case 501:
                        errorDomain = "Не реализовано"
                    case 503:
                        errorDomain = "Сервис недоступен"
                    case 300..<500:
                        errorDomain = "Неверный запрос"
                    case 500..<600:
                        errorDomain = "Ошибка сервера"
                    default:
                        errorDomain = "Неизвестная ошибка"
                    }
                    
                    return Request.ValidationResult.Failure(NSError(domain: errorDomain, code: status, userInfo: nil))
                })
                .responseString(completionHandler: { response in
                switch response.result {
                case .Success(let responseObject):
                    let data = responseObject.dataUsingEncoding(NSISOLatin1StringEncoding)

                    var dict: AnyObject? = nil
                    
                    do {
                        dict = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments)
                    } catch {
                        let error = NSError(domain: "Парсинг облажал", code: 0, userInfo: nil)
                        showError(error)
                        observer.sendFailed(error)
                    }
                    
                    let jsonDict = dict != nil ? JSON(dict!) : JSON(responseObject)
                    if jsonDict != nil {
                        if jsonDict["error"].dictionary != nil {
                            let error = NSError(domain: "Ошибка прислана в запросе", code: 1000, userInfo: nil)
                            observer.sendFailed(error)
                        }
                    }
                    if let token = request.response?.allHeaderFields["x-auth-token"] {
                        observer.sendNext(token)
                    } else {
                        observer.sendNext(jsonDict.dictionaryObject)
                    }
                    
                    observer.sendCompleted()

                case .Failure(let error):
                    showError(error)
                    observer.sendFailed(error)
                }
                
                disposable.addDisposable(request.cancel)
            })
        }
    }
}

func showError(error: NSError) {
    guard let topNotNil = UIApplication.topViewController() else { return }
    let alert = UIAlertController(title: "Ошибка \(error.code)", message: error.domain, preferredStyle: UIAlertControllerStyle.Alert)
    let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
    alert.addAction(action)
    topNotNil.presentViewController(alert, animated: true, completion: nil)
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension NSManagedObject {
    func toJSON() -> [String: AnyObject] {
        var dict = [String: AnyObject]()
        let attributes = self.entity.attributesByName
        
        for (key, _) in attributes {
            var value: AnyObject? = nil
            value = self.valueForKey(key)
            guard value != nil else { continue }
            
            if value is NSManagedObject {
                let value_ = (value as! NSManagedObject).toJSON()
                dict[key] = value_
            } else {
                dict[key] = value
            }
        }
        return dict
    }

    func safeSetValuesForKeysWithDictionary(keyedValues: [String: AnyObject]) {
        let attributes = self.entity.attributesByName
        for (key, attribute) in attributes {
            var newValue: AnyObject? = keyedValues[key]
            guard key != "descript" else { self.setValue(keyedValues["description"], forKey: key); continue }
            guard newValue != nil else { continue }
            let attributeType = attribute.attributeType
            switch attributeType {
            case .StringAttributeType:
                if newValue! is NSNumber { newValue = newValue!.stringValue }
            case .BooleanAttributeType:
                break
            case .Integer64AttributeType:
                break
            case .FloatAttributeType:
                break
            case .ObjectIDAttributeType:
                break
            default: break
            }
            guard !newValue!.isEqual(NSNull()) else { continue }
            self.setValue(newValue, forKey: key)
        }
        
        let relationships = self.entity.relationshipsByName
        for (key, _) in relationships {
            var newValue: AnyObject? = keyedValues[key]
            guard newValue != nil else { continue }
            let map = ["director": ProfileEntity.MR_createEntity()!,
                       "avatar": ImageEntity.MR_createEntity()!,
                       "originalImage": OriginalImageEntity.MR_createEntity()!,
                       "cropParams": CropParamsEntity.MR_createEntity()!,
                       "parentDepartment": BranchEntity.MR_createEntity()!,
                       "userPosition": UserPositionEntity.MR_createEntity()!,
                       "lastPostRead": PostEntity.MR_createEntity()!,
                       "userDepartment": BranchEntity.MR_createEntity()!]
            if map[key] != nil {
                let object = map[key]!
                object.safeSetValuesForKeysWithDictionary(newValue as! [String: AnyObject])
                newValue = object
            }
            self.setValue(newValue, forKey: key)
            
        }

    }
}
