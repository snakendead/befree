//
//  BranchEntity+CoreDataProperties.swift
//  Befree
//
//  Created by developer2 on 16.05.16.
//  Copyright © 2016 developer2. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension BranchEntity {

    @NSManaged var descript: String?
    @NSManaged var id: NSNumber?
    @NSManaged var name: String?
    @NSManaged var numberOfEmployees: NSNumber?
    @NSManaged var myBranch: NSNumber?
    @NSManaged var postsUnread: NSNumber?
    @NSManaged var avatar: ImageEntity?
    @NSManaged var director: ProfileEntity?
    @NSManaged var parentDepartment: BranchEntity?

}
