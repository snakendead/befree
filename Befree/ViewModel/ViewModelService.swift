//
//  ViewModelService.swift
//  Befree
//
//  Created by developer2 on 29.04.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

class ViewModelService: NSObject, ViewModelServiceProtocol {

    var service = ServiceLayer()
    var currentNavigationController: UINavigationController?
    var mainContainer: MainContainerViewController?

    func logout() {
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: tokenKey)
        setRootAuth()
    }

    func setRootAuth() {
        mainContainer = nil
        let authVM = AuthViewModel(viewModelService: self)
        let authVC = AuthViewController(viewModel: authVM)
        let navigationController = UINavigationController(rootViewController: authVC)
        navigationController.navigationBarHidden = true
        currentNavigationController = navigationController
        switchRootController(navigationController)
    }

    func setRootMainContainer() {
        let mainContainerVM = MainContainerViewModel(viewModelService: self)
        let mainContainerVC = MainContainerViewController(viewModel: mainContainerVM)
        mainContainer = mainContainerVC
        currentNavigationController = nil
        let profileVM = ProfileViewModel(viewModelService: self)
        setProfileNavigation(profileVM)
        switchRootController(mainContainerVC)
    }
    
    func setFeedNavigation() {
        let feedVM = FeedViewModel(viewModelService: self)
        let feedVC = FeedViewController(viewModel: feedVM)
        setAsContent(feedVC)
    }
    
    func setProfileNavigation(viewModel: ProfileViewModel) {
        let profileVC = ProfileViewController(viewModel: viewModel)
        setAsContent(profileVC)
    }
    
    func setBranchesNavigation() {
        let branchesVM = BranchesViewModel(viewModelService: self)
        let branchesVC = BranchesViewController(viewModel: branchesVM)
        setAsContent(branchesVC)
    }

    func pushBranch(viewModel: BranchViewModel) {
        let branchVC = BranchViewController(viewModel: viewModel)
        currentNavigationController?.pushViewController(branchVC, animated: true)
    }
    
    func pushBranchInfo(viewModel: BranchInfoViewModel) {
        let branchVC = BranchInfoViewController(viewModel: viewModel)
        currentNavigationController?.pushViewController(branchVC, animated: true)
    }
    
    func pushBranchWorkers(viewModel: WorkersViewModel) {
        let branchVC = WorkersViewController(viewModel: viewModel)
        currentNavigationController?.pushViewController(branchVC, animated: true)
    }

    private func setAsContent(viewController: UIViewController) {
        let navigationController = UINavigationController(rootViewController: viewController)
        currentNavigationController = navigationController
        mainContainer?.setNavigationViewController(navigationController)
    }
    
    private func switchRootController(viewController: UIViewController) {
        let window = UIApplication.sharedApplication().keyWindow!
        UIView.transitionWithView(window,
                                  duration: 0.33,
                                  options: UIViewAnimationOptions.TransitionCrossDissolve,
                                  animations: {
                                    window.rootViewController = viewController
                                }, completion: nil)
        
        
    }

    
}
