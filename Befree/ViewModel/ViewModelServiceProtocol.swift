//
//  ViewModelServiceProtocol.swift
//  Befree
//
//  Created by developer2 on 29.04.16.
//  Copyright © 2016 developer2. All rights reserved.
//

import UIKit

protocol ViewModelServiceProtocol {

    var service: ServiceLayer { get }
    
    func logout()
    
    func setRootAuth()
    func setRootMainContainer()

    func setFeedNavigation()
    func setProfileNavigation(viewModel: ProfileViewModel) 
    func setBranchesNavigation()
    
    func pushBranch(viewModel: BranchViewModel)
    func pushBranchInfo(viewModel: BranchInfoViewModel)
    func pushBranchWorkers(viewModel: WorkersViewModel)
}
