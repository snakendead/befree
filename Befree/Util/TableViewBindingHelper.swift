//
//  TableViewBindingHelper.swift
//  ReactiveSwiftFlickrSearch
//
//  Created by Colin Eberhardt on 15/07/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import Foundation
import ReactiveCocoa
import WebImage

class BranchHeaderView: UITableViewCell {
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
}

class FeedCell: UITableViewCell {
    @IBOutlet weak var backView: UIView!
}

class BranchCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    @IBOutlet weak var myBranchLabel: UILabel!
    @IBOutlet weak var myBranchConstraint: NSLayoutConstraint!
    @IBOutlet weak var countConstraint: NSLayoutConstraint!
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
}

class WorkerCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!

    @IBOutlet weak var profileImageView: UIImageView!
}

class TableViewBindingHelper: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    let imageUrl = "http://social.befree.ddgcorp.ru:11235"
    
    var delegate:                                           UITableViewDelegate?
    
    private let tableView:                                  UITableView
    private let templateCell:                               UITableViewCell

    private let data                                        = MutableProperty<[AnyObject]>([AnyObject]())
    private let reuseID                                     = MutableProperty<String>("")

    var (selectProducer, selectObserver)                    = SignalProducer<NSIndexPath, NSError>.buffer(5)
    var (deleteProducer, deleteObserver)                    = SignalProducer<NSIndexPath, NSError>.buffer(5)
    
    private let isDeleting                                  = MutableProperty<Bool>(false)
    
    var headerCell                                          : UITableViewCell?
    
    init(tableView:         UITableView,
         data:              SignalProducer<[AnyObject], NSError>,
         nibName:           String,
         header: UITableViewCell? = nil) {
        
        self.tableView = tableView
        self.reuseID.value = nibName
        let nib = UINib(nibName: reuseID.value, bundle: nil)

        templateCell = nib.instantiateWithOwner(nil, options: nil)[0] as! UITableViewCell
        self.tableView.registerNib(nib, forCellReuseIdentifier: reuseID.value)
        self.headerCell = header
        
        super.init()
        
        
        data.producer
            .startWithNext { [unowned self] next in
                self.data.value.appendContentsOf(next)
                self.tableView.reloadData()
        }

        tableView.alwaysBounceVertical = false
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if headerCell != nil {
            if headerCell is BranchHeaderView {
                (headerCell as? BranchHeaderView)?.profileImageView.clipsToBounds = true
                (headerCell as? BranchHeaderView)?.profileImageView.layer.cornerRadius = 50.0
            }
            return headerCell
        }
        return nil
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerCell == nil ? 0 : headerCell!.frame.height
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return self.isDeleting.value
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        guard editingStyle == UITableViewCellEditingStyle.Delete else { return }
        deleteObserver.sendNext(indexPath)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(data.value.count)
        return data.value.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(reuseID.value, forIndexPath: indexPath)

        cell.backgroundColor = UIColor.clearColor()
        cell.clipsToBounds = false
        cell.layer.masksToBounds = false

        let cellClassMap = ["FeedCell"      : feedCell,
                            "BranchCell"    : branchCell,
                            "WorkerCell"    : workerCell]

        let impl = cellClassMap[reuseID.value]!
        impl(&cell, indexPath: indexPath)
        
        return cell
    }
    
    func feedCell(inout cell: UITableViewCell, indexPath: NSIndexPath) {
        guard let feedCell = cell as? FeedCell else { return }
        feedCell.backView = UIView()
    }

    func branchCell(inout cell: UITableViewCell, indexPath: NSIndexPath) {
        guard let branchCell = cell as? BranchCell else { return }
        guard let branchEntities = self.data.value as? [BranchEntity] else { return }
        let branch = branchEntities[indexPath.row]
        branchCell.subtitle.text = "Участников \(branch.numberOfEmployees!)"
        branchCell.title.text = branch.name
        
        branchCell.profileImageView.layer.cornerRadius = branchCell.profileImageView.frame.height/2
        branchCell.profileImageView.clipsToBounds = true
        
        branchCell.myBranchLabel.layer.cornerRadius = branchCell.myBranchLabel.frame.height/2
        branchCell.myBranchLabel.clipsToBounds = true
        
        branchCell.myBranchConstraint.constant = branch.myBranch?.boolValue == false ? 0 : branchCell.myBranchConstraint.constant
        
        branchCell.countLabel.text = "\(branch.postsUnread!.intValue)"
        branchCell.countLabel.layer.cornerRadius = branchCell.countLabel.frame.height/2
        branchCell.countLabel.clipsToBounds = true
        
        let attributedText = NSAttributedString(string: branchCell.countLabel.text!, attributes: [NSFontAttributeName:branchCell.countLabel.font])
        let textSize = attributedText.boundingRectWithSize(CGSizeMake(1000, 1000), options: NSStringDrawingOptions.UsesFontLeading, context: nil)
        let textWidth: CGFloat = textSize.width
        let newWidth: CGFloat = textWidth + MySizes.smallOffset*2
        let width: CGFloat = branchCell.countLabel.frame.height > newWidth ? branchCell.countLabel.frame.height : newWidth
        branchCell.countConstraint.constant = branch.postsUnread?.intValue == 0 ? 0 : width
        
        guard let preview = branch.avatar?.previewNormal else { return }
        guard let url = NSURL(string: imageUrl + preview) else { return }
        branchCell.profileImageView.sd_setImageWithURL(url, placeholderImage: nil, options: SDWebImageOptions.ProgressiveDownload)
    }

    func workerCell(inout cell: UITableViewCell, indexPath: NSIndexPath) {
        guard let workerCell = cell as? WorkerCell else { return }
        guard let entities = self.data.value as? [ProfileEntity] else { return }
        let workerProfile = entities[indexPath.row]
        workerCell.subtitle.text = workerProfile.userPosition?.name
        workerCell.title.text = safeString(workerProfile.firstName) + " " + safeString(workerProfile.lastName)
        
        workerCell.profileImageView.layer.cornerRadius = workerCell.profileImageView.frame.height/2
        workerCell.profileImageView.clipsToBounds = true

        guard let preview = workerProfile.avatar?.previewNormal else { return }
        guard let url = NSURL(string: imageUrl + preview) else { return }
        workerCell.profileImageView.sd_setImageWithURL(url, placeholderImage: nil, options: SDWebImageOptions.ProgressiveDownload)
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return templateCell.frame.size.height
    }
  
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectObserver.sendNext(indexPath)
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        guard self.delegate?.respondsToSelector(#selector(UIScrollViewDelegate.scrollViewDidScroll(_:))) == true else { return }
        self.delegate?.scrollViewDidScroll?(scrollView)
    }
}
